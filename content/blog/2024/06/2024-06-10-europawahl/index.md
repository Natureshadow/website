+++
title = "Über die demokratische Verantwortung"
description = ""

authors = ["Dominik George"]

[taxonomies]
themen = ["Pädagogik"]
#projekte = [""]
tags = ["politik", "demokratie", "verantwortung"]
+++

Am 9. Juni war Europawahl. In Deutschland waren knapp 65 Millionen Menschen
dazu aufgerufen, ihre Stimme für die Sitzverteilung im Europaparlament für
die nächsten fünf Jahre abzugeben. Am Abend des Wahltages fühlten dann eine
Menge Demokrati\*innen tiefe Enttäuschung: Die als rechtsextrem eingestufte
AfD konnte deutlich mehr Wähler\*innen als erhofft von ihrem Programm überzeugen
und erhielt, insbesondere bei jungen Menschen sowie in den östlichen Bundesländern,
Rekordzuspruch. In fast allen Wahlkreisen in Mecklenburg-Vorpommern, Brandenburg,
Sachsen, Sachsen-Anhalt und Thüringen sowie in den ehemaligen Ostberliner Bezirken
stimmte sogar die relative Mehrheit für die Wahllisten der AfD, im Schnitt etwa ein
Drittel der Wähler\*innen.

Verständlich, dass viele Menschen, insbesondere diejenigen, denen durch die Politik
der AfD eine direkte Gefährdung drohen würde, auf die Ergebnisse mit Angst reagieren.
Immerhin geht es unter Umständen um nicht weniger als ihr Überleben. Viele haben die
Konsequenzen aus einer zu schwachen oder ausbleibenden Reaktion auf die Klimakrise
oder durch menschenverachtende Asylpolitik deutlich vor Augen und skizzieren die
Szenarien der nächsten Jahre und Jahrzehnte detailliert und weitsichtig.

Allen ist klar: Jetzt muss etwas getan werden. Die Proteste und Demonstrationen, die
Werbung für weltoffene, humaistische und ökologisch nachhaltige Politik, all das war
nicht genug, um ausreichend viele zu überzeugen. Und da liegt eine Schlussfolgerung doch
nahe: *In Ostdeutschland ist jede\*r Dritte ein Faschist.*

Und mit Fachisten, das hat die Geschichte gezeigt, redet man nicht. Eine, wie ich finde,
ehrenwerte und im Kern gute Haltung, die in vielen Bereichen ein wertvoller Leitsatz
sein kann. Beispielsweise im öffentlichen Rundfunk, der den Politiker\*innen der AfD
seit geraumer Zeit eine breite Bühne bietet. Doch was taugt dieser Leitsatz im Umgang
mit dem Einzelnen? *Wer Faschisten wählt, ist ein Faschist* – folgerichtig müssen wir
also ein Drittel der Bevölkerungin den ostdeutschen Ländern, sowie ein Sechstel der
Menschen zwischen 16 und 24 Jahren, aus der Gesellschaft verbannen, nicht mehr mit ihnen
reden, kurz: Ihnen Grenzen setzen, die sie ausgrenzen.

Treten wir ein paar Schritte zurück und überlegen uns einmal, was denn eigentlich die
meisten dieser Menschen dazu bewogen haben könnte, ihre Stimme ausgerechnet für eine
antidemokratische und, in weiten Teilen, gesichert rechtsextremistischee Partei abzugeben.
Es ist einfach, anzunehmen, dass sich alle diese Menschen das dritte Reich zurückwünschen,
aktiv Asylant\*innen und Flüchtlingen Schaden zufügen wollen oder Spaß an Überflutungen,
Extremwettern und Hungersnöten haben. Alle diese Folgerungen habe ich, teils wörtlich,
im Fediverse gelesen. Dieses Menschenbild lehne ich jedoch ab – im Allgemeinen möchte
kein Mensch einem anderen oder gleich der ganzen Gesellschaft aus reinem Selbstzweck oder
aus Spaß Schaden zufügen. Viel mehr kann ich mich da mit dem Interpretationsansatz
anfreunden, dass so gut wie alle AfD-Wähler\*innen ihre Entscheidung aus egoistischen
Gründen getroffen haben.

Doch was bedeutet Egoismus? Neben dem Begriff des Egoismus gibt es auch noch den des
Egozentrismus. Beide Begriffe sind weitestgehend negativ beladen, denn der Einsatz für die
Gemeinschaft und das Zurückstellen eigener Bedürfnisse wird weitestgehend als "gut" und
moralisch richtig betrachtet. Tatsächlich ist Egozentrismus, in seiner reinen Form, jedoch
eine wichtige Fähigkeit: Sie erlaubt es, seine eigenen Bedürfnisse und Grenzen, seine
Integrität und seine Kooperationsbereitschaft, zu erkennen und, im besten Fall, zu kommunizieren,
ohne sich dabei selber zu verletzen oder unerträglichen Verletzungen durch andere auszusetzen.

Idealerweise erlernt ein Mensch, in seiner Familie und später in anderen Gemeinschaften, wie
weit er seine eigenen Bedürfnisse zum Wohle der Gemeinschaft zurückstellen kann, ohne dabei
seine Integrität unerträglich zu verletzen. In der Realität sind wir von diesem Ideal weit
entfernt: Einige Familien und besonders viele Schulen respektieren den selbstbestimmten Umgang
mit Grenzen nicht in ausreichendem Maße, bestrafen aus ihrer mangelnde Kooperation und erziehen,
absichtlich oder unabsichtlich, ein selbstzerstörerisches Verhalten an. Die Kompensation dieses
Verhaltens durch rücksichtslosen Egozentrismus, also die Verletzung anderer zu seinen Gunsten,
nennt man Egoismus.

Ein wichtiger Aspekt beim Erlernen dieses Selbstgefühls ist es, die Konsequenzen seines Handelns
zeitlich oder räumlich immer weitreichender überblicken zu können. Dabei kommt es auch auf die
Unterscheidung zwischen spontanen Lüsten und tatsächlichen Bedürfnissen an, vor allem aber darauf,
Bedürfnisse vorauszusehen. Um das zu können, muss man sich selber schon sehr gut kennengelernt haben
und außerdem mit dem Gedanken leben können, über einen längeren Zeitraum auf die Erfüllung der eigenen
Wünsche und Bedürfnisse verzichten zu können, den Schmerz darüber auszuhalten. Diesen Konflikt
kann man internalisiert, also mit sich selber, austragen, oder aber im Dialog mit anderen.

Und genau da setzt die Strategie der AfD psychologisch an: Ihr Programm, und dabei an erster Stelle
das ihrer regionalen Vertreter\*innen, ist darauf ausgerichtet, spontane Wünsche und teilweise auch
Bedürfnisse anzusprechen. Zu diesen Bedürfnissen gehört sicherlich auch, möglichst keine Veränderung
der persönlichen Lebensrealität in Kauf nehmen zu müssen (im Bezug auf bspw. Klimapolitik) oder eine
einfache Erklärung für Probleme in Lebensbereichen zu finden, in denen man selber zu wenig Macht hat
(bspw. Asylpolitik). Andere Parteien hingegen sind darauf ausgerichtet, eine nachhaltige Strategie
für uns als Gesellschaft zu vertreten. Und damit sind sie, in gewisser Hinsicht, ableistisch, denn
wie eben erörtert ist ihre Politik damit nur denjenigen wirklich zugänglich, die die Fähigkeit zur
Selbstregulation zwischen Integrität und Verzicht in ausreichendem Umfang erlernt haben – genau das,
worin wir als Gesellschaft aber mit am schlechtesten sind.

Vor zwei Wochen hatte ich mal wieder die Gelegenheit, auf einer längeren Fahrradtour größere Teile von
Sachsen, Sachsen-Anhalt und Thüringen anzusehen. Die meisten werden ein Bild vieler Städte dort vor
Augen haben: Verbarrikadierte, leerstehende Häuser, Ruinen, kaum sanierte Straßen prägen noch immer
das Stadtbild. Was ich auf Lost-Place-Touren wildromantisch finde, hat auf mich während meiner letzten
Tour einen teils beklemmenden Eindruck gemacht. Besonders stark war dieses Gefühl an einem Abend in
Falkenstein. Eine weitestgehend leere Stadt, im Großen und Ganzen dem oben skizzierten Bild entsprechend,
starker Bodennebel tat sein übriges. Und im Hotelzimmer sah es auch nicht besser aus. Ich hätte gerne
mit jemandem geredet, oder zumindest irgendwo die Nähe anderer Menschen wahrgenommen. Selbst, wenn ich
alleine in einer Bar gesessen hätte, mit anderen, redenden Menschen an den Nachbartischen, hätte ich
mich besser gefühlt. Aber da war absolut nichts. Dieses beklemmende, unsichere Gefühl konnte ich erst
wieder ablegen, als ich am nächsten Abend in Chem (Eger) in einem Brauhaus saß. Ebenfalls alleine, aber
wahrgenommen, und in der Nähe anderer Menschen.

Wenn sich dieser Eindruck auch nur ansatzweise auf das alltägliche Lebensgefühl der Einwohner\*innen
übertragen lässt, dann erscheint es plötzlich deutlich weniger fraglich, wieso das Augenmerk hier
eher auf einer akuten, versprochenen, wenn auch voraussichtlich nicht eintreffenden, Zustandsverbesserung
liegt. Zweifelsohne ist die Politik der AfD nicht der richtige Weg zu dieser Verbesserung, aber, für
den persönlichen Konflikt, eben der mutmaßlich schnellste und einfachste. Weder die Durchschnittstemperatur
auf der Erde in 30 Jahren noch die Öffnung für mehr Pflegepersonal aus dem Ausland wird auch nur für
einen dieser Menschen eine akute Verbesserung der Lebensrealität herbeiführen. Und, mit Verlaub, keine
der demokratischen Parteien hat das im Wahlkampf auch nur versucht, zu versprechen.

Zusammengefasst: Die Herausforderungen, vor denen wir gesellschaftlich stehen, sind zu groß, zu räumlich
und zeitlich weitreichend, um in der Konfliktbearbeitung des Einzelnen ausreichend gewürdigt werden zu
können. Das doch zu schaffen, und seine akuten, dringenden Bedürfnisse so weit zurückzustellen, den Schmerz
ihrer Nichtbefriedigung auszuhalten, ist ein Privileg.

Möchte man beurteilen, wer in einem Konflikt die größte Verantwortung für eine gleichwürdige Lösung trägt,
dann sucht man nicht den Schuldigeren, sondern den Mächtigeren. Die oder der Mächtigere trägt die Verantwortung
dafür, dass bei der Lösung eines Konflikts alle Beteiligten ihre Integrität mit den kleinstmöglichen
Verletzungen wahren können. Und obwohl in einer repräsentativ demokratischen Republik natürlich auf dem Papier
die Gesamtheit der Wähler\*innen der Souverän ist und damit die uneingeschränkte Macht haben sollte, ist das
weder real noch emotional der Fall. Wenigstens auf die Minderheiten, die Wähler\*innen der Oppositionsparteien,
wirkt die Regierung als Souverän. Aussicht auf Änderung gibt es erst bei der nächsten Parlamentswahl.

Während nun also jede\*r Einzelne ein Kreuz auf dem Wahlzettel macht und damit die zu diesem Zeitpunkt
für sie oder ihn beste Lösung des politischen Konflikts manifestiert, trägt die Hauptverantwortung für die
Qualität dieser Entscheidung, also ob die Entscheidung konstruktiv oder destruktiv ist, moralisch im Sinne der
Gesellschaft ist oder nicht, doch der Mächtigere Partner. Und das sind in der Politik die Politiker\*innen, nicht
die einzelnen Bürger\*innen.

Zu den Politiker\*innen können wir auch alle diejenigen zählen, die das Privileg genießen, ihre politischen
Entscheidungen weitestgehend unabhängig von ihren akutesten Bedürfnissen treffen zu können. Und da müssen wir, angesichts
der Wahlergebnisse, Verantwortung übernehmen und klar formulieren: **Wir haben es kollektiv vergeigt.**

Mit Blick nach vorne zählt es jetzt, daraus zu lernen und zu überlegen, wie wir unserer Verantwortung auf neue Art
gerecht werden können. Dazu gehört Aufklärung über Bildung und den ÖRR, aber eben auch, zu überlegen, wie wir den
Menschen, deren eigenes Konfliktlösungspotential nicht für die zeitliche und räumliche Distanz der gesellschaftlichen
Probleme reicht, bei der Befriedigung ihrer Bedürfnisse entgegenkommen können. Und mit Entgegenkommen meine ich hier
keine bedingungslose Akzeptanz, sondern ihre Anerkennung und das Angebot adäquater, moralisch vertretbarer Wege zur
Erfüllung. Das erfordert auch Aktivismus auf diesem Gebiet – bisher sehe ich jedoch hauptsächlich Reaktionismus, wenn
man als Politiker\*in feststellt, dass man durch zu weit gefasste Abstraktion Ängste geschürt hat und dann gegensteuern
will. Für mich wäre dieser Aktivismus bei meinem Besuch in Falkenstein so einfach gewesen wie das Eröffnen eines ALEX
oder einer Bar Celona, in der Breite wird es weit komplexer sein.

Macht verantwortungsvoll einzusetzen, ist dabei wohl die wichtigste Tugend. Das sollte insbesondere die Medien,
darunter das öffentlich-rechtliche Fernsehen, die Presse, und auch soziale Medien wie TikTok oder Instagram, dazu
bewegen, ihre Verantwortung zu überdenken. Bei vielen werden wir feststellen, dass sie längst zum Machtapparat der
Populisten geworden sind – werden dabei antidemokratische Positionen mächtig, ist der richtige Zeitpunkt für Regulierung
im privatwirtschaftlichen Bereich oder auch im Sinne unserer Verfassung für ein Parteienverbot gekommen.

Nichts davon ist eine Rechtfertigung für konkrete menschenverachtende, demokratiefeindliche oder klimaschädliche
Sichtweisen oder Handlungen. Doch diese Handlungen und Sichtweisen vertritt, so meine Überzeugung, noch immer eine
Minderheit, eine Minderheit der Mächtigen. Und diese Mächtigen darf man ohne Weiteres als das bezeichnen, was sie sind.
Wer daraus aber auf die Mehrheit der Wähler\*innen schließt, denkt nicht weit genug.

Ich bin überzeugt: **Die wenigsten AfD-Wähler\*innen sind Faschisten.**
