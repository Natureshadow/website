+++
title = "Auf Radtour durch Dänemark: An der Ostküste bis Århus"
description = ""

authors = ["Dominik George"]

[taxonomies]
themen = ["Reisen"]
#projekte = [""]
tags = ["daenemark", "foss", "skandinavien"]


[extra.head.libs]
leaflet = true
leaflet-gpx = true


[[extra.galleries.toender_schools.images]]
asset = "toender-schools-1.jpg"
alt = "Blick auf den Schulhof der Marieskole Tønder"

[[extra.galleries.toender_schools.images]]
asset = "toender-schools-2.jpg"
alt = "Blick auf den Schulhof der Marieskole Tønder"

[[extra.galleries.toender_schools.images]]
asset = "toender-schools-3.jpg"
alt = "Blick auf ein Waldstück, das zum Schulhof der Tønder Overbygningsskole"


[[extra.galleries.toender_haderslev.images]]
asset = "toender-haderslev-1.jpg"
alt = "Kirche in Ravsted"

[[extra.galleries.toender_haderslev.images]]
asset = "toender-haderslev-2.jpg"
alt = "Baggersee kurz vor Aabenraa"

[[extra.galleries.toender_haderslev.images]]
asset = "toender-haderslev-3.jpg"
alt = "Schlossmühle in Aabenraa"

[[extra.galleries.toender_haderslev.images]]
asset = "toender-haderslev-4.jpg"
alt = "Strand von Sønderballe"

[[extra.galleries.toender_haderslev.images]]
asset = "toender-haderslev-5.jpg"
alt = "Kirche von Vilstrup"

[[extra.galleries.toender_haderslev.images]]
asset = "toender-haderslev-6.jpg"
alt = "Kreisverkehr in Haderslev"


[extra.galleries.haderslev]
sizes = { desktop = "half" }

[[extra.galleries.haderslev.images]]
asset = "haderslev-1.jpg"
alt = "Blick über den Opfersee in Ejsbøl"

[[extra.galleries.haderslev.images]]
asset = "haderslev-2.jpg"
alt = "Sonnenuntergang in Ejsbøl"


[[extra.galleries.haderslev_vejle.images]]
asset = "haderslev-vejle-1.jpg"
alt = "Koldinghuset, die Burg von Kolding"

[[extra.galleries.haderslev_vejle.images]]
asset = "haderslev-vejle-2.jpg"
alt = "Yachthafen von Vejle"

[[extra.galleries.haderslev_vejle.images]]
asset = "haderslev-vejle-3.jpg"
alt = "Vejlefjord mit Brücke"


[[extra.galleries.vejle_aarhus.images]]
asset = "vejle-aarhus-1.jpg"
alt = "Radroute am Rapsfeld"

[[extra.galleries.vejle_aarhus.images]]
asset = "vejle-aarhus-2.jpg"
alt = "Rinder"

[[extra.galleries.vejle_aarhus.images]]
asset = "vejle-aarhus-3.jpg"
alt = "Windmühle an einer Landstraße"

[[extra.galleries.vejle_aarhus.images]]
asset = "vejle-aarhus-4.jpg"
alt = "Schotterweg direkt an der Küste"

[[extra.galleries.vejle_aarhus.images]]
asset = "vejle-aarhus-5.jpg"
alt = "Radweg im Wald vor Århus"

[[extra.galleries.vejle_aarhus.images]]
asset = "vejle-aarhus-6.jpg"
alt = "Yachthafen von Århus im Dunkeln"

[extra.galleries.essen]
sizes = { desktop = "one-quarter" }

[[extra.galleries.essen.images]]
asset = "essen-1.jpg"
alt = "Stjerneskud im Café Viktoria"

[[extra.galleries.essen.images]]
asset = "essen-2.jpg"
alt = "Meeresfrüchte"

[[extra.galleries.essen.images]]
asset = "essen-3.jpg"
alt = "Brunch in Kolding"

[[extra.galleries.essen.images]]
asset = "essen-4.jpg"
alt = "Frühstück am Hafen von Århus"
+++

Nun ist es so weit: Meine erste diesjährige Radtour beginnt. Um 9 Uhr bin ich
in Bonn mit dem Zug losgefahren – erst nach Köln, und von dort
[weiter nach Hamburg](https://traewelling.de/status/1030568). Das Ziel ist
heute [Tønder](https://de.wikipedia.org/wiki/T%C3%B8nder) kurz hinter der
deutsch-dänischen Grenze. Dort war ich letztes Jahr schon einmal, als ich
mit dem Fahrrad eine Rundtour um Nord-Schleswig-Holstein gemacht habe.

{{ map(height="600px", lat=55.5651, lon=8.9401, zoom=8) }}
{{ map_gpx(path="day-1.gpx") }}
{{ map_gpx(path="day-2.gpx") }}
{{ map_gpx(path="day-3.gpx") }}
{{ map_gpx(path="day-4.gpx") }}

Genau wie letztes Jahr bin ich übrigens auch diesmal wieder anlässlich der
[Debbian Reunion Hamburg](https://wiki.debian.org/DebianEvents/de/2023/DebianReunionHamburg)
und des [AlekSIS](https://aleksis.org/de/)-Entwickler-Sprints in Lübeck
unterwegs.

## Tag 1 - Im Zug nach Tønder

Die Bahnfahrt war überraschend entspannt. Im ersten Wagen, wo auch das
Fahrrad-Abteil ist, war wenig los, die vorhergesagte "hohe Auslastung"
spielte sich weier hinten ab, wo unter anderem eine Gruppe Besoffener,
wie immer vom Zugbegleitpersonal unbehelligt, ihrer Belästigung mit
Bluetooth-Lautspechern nachgingen. Ganz vorne hingegen erwartete mich
eine ziemlich unerwartete Überraschung: Ein Mitreisender mit ebenfalls
langen Haaren, Laptop und Vierkant bot sich an, mir einen Kaffee aus dem
Bordbistro mitzubringen.
vejle_aarhus
Als er mir seine E-Mail-Adresse für PayPal nannte, kam mir da doch irgendwas
bekannt vor: "Moment, du bist …?" Es stellte sich heraus, dass ich sowohl
Software von ihm benutze, als auch regelmäßig Mailinglisten-Posts von ihm sehe.
So ließ ich dann auch meine Arbeit, mal wieder, liegen, und es gab erstmal
einige Fachsimpelei, unter anderem über [F/reifunk](https://freifunk.net/).

Während der Fahrt bestückte ich auch meine Uhr mit der aktuellen
[Velomap](https://www.velomap.org/de/) für Deutschland und Dänemark. Gerade
rechtzeitig, denn durch die 15 Minuten Verspätung würde ich meinen Anschlusszug
von Niebüll nach Tønder verpassen, so dass ich diese Strecke dann doch
schon mit dem Fahrrad fahren musste.

Der "echte Süden" Dänemarks - also direkt an der Grenze zu Schleswig-Holstein -
ist erstmal vom "echten Norden" Schleswig-Holstein nicht so richtig zu
unterscheiden. Die meisten sprechen deutsch, Cafés akzeptieren, neben Dänischen
Kronen, auch Euro, und landschaftlich tut sich auch noch nicht so viel.

Eine Sache war mir aber schon letztes Jahr in Tønder aufgefallen: Mein erster
Eindruck von Schulen in Dänemark. Durch [KidZ](https://kinder-in-das-zentrum.de/)
hatte ich schon die eine oder andere Anmerkung über die größere Offenheit
dänischer Schulen mitbekommen, und so fiel mir das auch dann vielleicht direkt
ins Auge. Hier mal ein paar Fotos von zwei Schulen in Tønder:

{{ gallery(id="toender_schools") }}

Natürlich ist das nicht repräsentativ für das ganze Land, aber mir sprang
direkt ins Auge, dass dem Schulhof der [Marieskole](https://www.marieskolen.dk/)
eines komplett fehlt: ein Zaun. Der Schulhof befindet sich direkt in einem
öffentlichen Park, die Schule ist einfach Teil der Umgebung. Nun ist die
Marienschule eine Privatschule, aber auch die nahegelegene öffentliche
Overbygningsskole (Mittelschule) wirkt offener. Zum ausladenden Schulgelände
gehört ein Wald mit Tümpel.

Das Abendessen gab es im [Café Viktoria](https://victoriatoender.dk/), einer
kleinen, gemütlichen Bar direkt in der Altstadt von Tønder. Das hatte ich auch
2022 schon erprobt, und das Stjerneskud dort war einer der Hauptgründe für meine
Tour ;). Diesmal habe ich dann im
[Danhostel Tønder](https://www.danhostel.dk/de/hostel/danhostel-toender) übernachtet.
Es gehört zu einer großen Anlage aus Sportanlagen, einem Campingplatz und einem
Jugendtreff, im Wseentlichen dem Frezeitzentrum von Tønder. Trotzdem war es sehr
ruhig.

## Tag 2 - Von Tønder nach Haderslev

Zunächst verlief meine Route in Richtung Osten quer durch das Land. Wie immer, wenn
man in Richtung Meer fährt, kam der Wind natürlich konstant von vorne (in Dänemark
fährt man übrigens auch immer in Richtung Meer).

Auf dem Weg nach [Aabenraa](https://de.wikipedia.org/wiki/Aabenraa) begegneten mir
vor allem Rapsfelder und Kirchen. Es gibt, so mein Eindruck, nirgendwo so viel Raps
wie in Dänemark, zumindest das dänische Festland scheint zum größten Teil aus Raps
zu bestehen. Und natürlich gibt es viele Gewässer - Fjorde und Seen, nicht so viel
und groß wie in den nördlicheren Gebieten Skandinaviens, aber weit kommt man schon
nicht ohnene einen Tümpel oder See.

Vor Aabenraa, der ersten größeren Stadt, machte ich eine Pause an einem renaturierten
Steinbruch (Baggersee). Aabenraa selber hat mich nicht sehr begeistert, es ist irgendwie
eine eher unscheinbare, dafür recht hässliche, Hafenstadt. Das besondere an Städten
in Dänemark ist aber, dass die Hässlichkeit nicht besonders weit reicht. Die
Industriegebiete des Hafens sind so groß, wie sie sein müssen, und dann ist es auch
direkt wieder grün.

{{ gallery(id="toender_haderslev") }}

Das letzte Drittel verlief dann kurz am Aabenraa-Fjord entlang und dann, endlich,
Richtung Norden nach [Haderslev](https://de.wikipedia.org/wiki/Haderslev). Zum Ende
der Etappe konnte ich noch einmal die dänische Radinfrastruktur bestaunen: Die gut
ausgebauten nationalen Radrouten werden normalerweise sehr durchdacht in die Städte
geführt. Hier in Haderslev werden die ankommenden Radrouten im Süden, genau so wie
der Autoverkehr, in einen Kreisverkehr geführt. Doch das Radwegenetz hat seinen
eigenen Kreisverkehr, der in das Innere des Auto-Kreisverkehrs eingebettet ist. Man
durchfährt eine Unterführung unter der Autostraße hindurch ins Innere des Kreisverkehrs,
wo dann gut beschilderte Routen in die Stadt oder in andere Richtungen führen.

Mein Bed & Breakfast lag nördlich etwas außerhalb im Vorort Moltrup. Deshalb musste
ich zum Abendessen auch noch einmal etwa 5km zurück in die Innenstadt von Haderslev.
Dabei entdeckte ich unterwegs noch den eisenzeitlichen Opfersee von Ejsbøl, in dem
in der Eisenzeit Waffenopfer erbracht wurden.

{{ gallery(id="haderslev") }}

## Tag 3 - Von Haderslev nach Vejle

Frühstück gab es im Bed & Breakfast heute nicht (<span lang="en-GB">hence the name</span>),
und so legte ich zunächst einen ziemlichen Sprint nach
[Kolding](https://de.wikipedia.org/wiki/Kolding) ein. Dort kam ich gerade noch rechtzeitig
zum kleinen Brunch mit Rührei, Würstchen, Pancakes und Käse im [STAYcafé](https://staycafe.dk/)
an und konnte noch kurz die Burg anschauen.

{{ gallery(id="haderslev_vejle") }}

Schon gegen 15 Uhr kam ich dann in [Vejle](https://de.wikipedia.org/wiki/Vejle), meinem
Etappenziel, an. Vejle liegt schon sehr nah am Meer, der Vejlefjord ist ein ziemlich breites
Gewässer. Und im Gegensatz zu Aabenraa fand ich auch das Hafengebiet nicht ganz so hässlich.

Mein Bed & Breakfast, direkt neben der Vejlefjordbrücke, war hervorragend. Die Inhaberin
hatte früher in einem Hostel in Århus gearbeitet und entsprechend Erfahrung. Ich bekam
ein großes, gemütliches Zimmer und am Morgen ein wirklich umfangreiches Fühstück.

## Tag 4 - Von Vejle nach Århus

Zum letzten Tag meiner Tour sollte es dann nach [Århus](https://de.wikipedia.org/wiki/Aarhus),
zweitgrößte Stadt Dänemarks und Hauptziel meiner Tour, gehen. In
[Horsens](https://de.wikipedia.org/wiki/Horsens), einer wirklich schönen Stadt mit ruhiger
Einkaufs- und Fußgängerzone, kehrte ich noch in einem Café ein - etwas hipster, denn hier
gan es Roggenbrot mit Krabben und Rotbarschfilet, aber halt eben Rotbarsch statt Scholle und
man wollte es nicht Stjerneskud nennen ;).

Während des Essens kam mir dann auch noch die großartige Idee, dass ich meine Route noch
etwas umplanen könnte. Eigentlich wollte ich wieder weitestgehend durchs Hinterland über
die Dörfer fahren, doch für meinen Geschmack hatte ich bisher zu wenig Küste gesehen. Und
so entdeckte ich die [Ostküstenroute](https://www.kystlandet.dk/oplevelser/cykelferie/oestkystruten),
eine von 11 nationalen Fernradrouten Dänemarks. Dieser Route war ich zwischendurch schon
teilweise gefolgt, und so fasste ich nun den Entschluss, bis Århus nun ganz an der Küste
entlang zu fahren.

{{ gallery(id="vejle_aarhus") }}

Die Entscheidung habe ich nicht bereut. Die Strecke verlängerte sich dadurch auf fast
100 Kilometer, aber es war auch der schönste Abschnitt der ganzen Tour. Etwa 10 Kilometer
vor dem Ziel verlief der Weg durch ein großes Waldgebiet (ich erwähnte ja bereits, dass
selbst die großen Städte mittem im Grünen liegen) mit direktem Blick aufs Meer. Schattig
und gut ausgebaut gign es bis direkt zum Hafen, wo ich gegen 17:30 Uhr mein Hotel erreichte.

Von den 100 Kilometern noch nicht ganz ausgelastet, schaute ich mir dann noch zu Fuß die
Innenstadt an und wurde Opfer eines "guten" Restaurants mit recht kleinen Preisen, und noch
viel kleineren Portionen.

## Tag 5 - Rückfahrt nach Hamburg mit dem Flixbus

Eine größere Herausforderung ist es, mitsamt Fahrrad wieder zurückzureisen. Die Auslastung
der dänsichen Züge mit Fahrrädern ist noch höher als in Deutschland, und so landete ich
diesmal beim Fernreisebus. Zum Frühstück gab ich noch meine letzten dänischen Kronen im
Surfcafé am Hafen aus, und dann machte ich mich auf den Weg zum Busbahnhof.

Fas pünktlich fuhr der Bus dann um 11:10 Uhr ab, mit geplanter Ankunft in Hamburg um
15:45 Uhr. Die Fahrt war eigentlich vollkommen in Ordnung, aber trotzdem nicht besonders
angenehm, da ich insgesamt keine Autobahnen mag.

## Rückblick

Was eigentlich aus der Motivation "Deutschland wird langsam langweilig" entstanden ist,
wurde zur wohl schönsten Radtour bisher. Dänemark ist ein tolles Land, vor allem wegen
der abwechslungsrecihen Landschaft. Und - möglicherweise romantisiere ich das etwas -
die Dänen scheinen ein anderes Verhältnis zu ihrem Land und ihrer Umwelt zu haben.
Insbesondere fiel mir rückblickend auf, dass ich insgesamt kaum Verschmutzung gesehen
habe. Klar, besonders in Århus, was ja durchaus Ballungsgebiet ist, gab es die eine oder
andere beschmierte Hauswand, aber beispielsweise der Grillplatz im Wald südlich der Stadt
war komplett sauber; auch an ähnlichen Orten auf der Route war kein Müll oder ähnliches
zu finden.

Radfahren macht in Dänemark insgesamt viel Spaß. Es ist nicht alles perfekt, aber man
merkt doch, dass man als Radfahrer ähnlich wertgeschätzt wird wie als Autofahrer,
vielleicht sogar etwas mehr. Ich bin auch an Straßen ohne Radweg gefahren, aber das
waren dann meistens kleinere Landstraßen, auf denen kaum Verkehr war. An den größeren
Straßen gibt es meistens Radwege, die genau so gut ausgebaut sind wie die Straße, oft
breit und zweispurig. Und insgesamt habe ich auf meinen 270 Kilometern, auf denen ich
7 größere Städte duchquert habe, nur 5 Mal an Ampeln gewartet und mich 2 Mal über einen
Autofahrer geärgert (Idioten gibt es eben überall).

Deshalb: Die Tour wird nicht meine letzte gewesen sein. Als nächstes geht es entweder
von Århus nach Aalborg oder von Kolding nach København. Ultimativ schwebt mir vor,
dann später einmal mit der Fähre nach Göteborg in Schweden und von dort mit dem
Fahrrad nach Oslo zu fahren.

Fahrradurlaub ist etwas Besonderes - es macht am Ende des Tages den Unterschied, ob
man das Land kennengelernt hat oder die Autobahnen. Ach ja, und das Essen schmeckt
hungrig auch noch viel besser!

{{ gallery(id="essen") }}
