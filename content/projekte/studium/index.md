+++
title = "Pädagogik-Studium"
description = "Etwas verspätet habe ich 2023 ein ernsthaftes Studium begonnen, um meine bisherige professionelle Erfahrung wissenschaftlich zu untermauern."

[taxonomies]
themen = ["Pädagogik"]

[extra]
icon = "fa-solid fa-graduation-cap"
+++

Zwei grundlegende Fakten über mich kennt eigentlich jede\*r, der mehr als
nur gelegentlich mit mir zu tun hat:

1. Ich beschäftige mich leidenschaftlich mit allem, was mit Kindern,
   Erziehung und insbesondere Kinderrechten zu tun hat.
2. Bis zu meinem 32. Lebensjahr habe ich insgesamt drei Studiengänge
   begonnen und dann lustlos wieder abgebrochen.

Was das eine mit dem anderen zu tun hat, weshalb ich fast 20 Jahre lang
ohne formelle Ausbildung oder wissenschaftliches Studium in der
Jugendarbeit aktiv war und was nun zum Studium geführt hat, erzähle ich
in meinem [Einsteigs-Blogpost zum Pädagogik-Studium](@/blog/2023/04/2023-04-08-lernblog-studium.md).
