+++
title = "Reisen und Touren"
description = "Nicht nur zu Konferenzen, auch privat bereise ich gerne die Welt per Rad und Bahn."

[taxonomies]
#projekte = [""]

[extra]
icon = "fa-solid fa-mountain-city"

[extra.head.libs]
leaflet = true
leaflet-gpx = true
+++
