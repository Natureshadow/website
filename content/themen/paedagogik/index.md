+++
title = "Pädagogik"
description = "Die Begleitung anderer, meist jüngerer, Menschen in ihrer Entwicklung hat mich schon in meiner eigenen Schulzeit begeistert. Seitdem bin ich in verschiedensten pädagogischen Projekten aktiv."

[taxonomies]
projekte = ["Studium"]

[extra]
icon = "fa-solid fa-hands-holding-child"
+++
